<?php

namespace App\Entity;

use App\Entity\Account;
use App\Entity\Categoryevent;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Event
 *
 * @ORM\Table(name="event", indexes={@ORM\Index(name="fk_event_cat_idx", columns={"id_event_cat"}), @ORM\Index(name="fk_event_account_idx", columns={"id_event_account"})})
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     *  @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your name must be at least {{ limit }} characters long",
     *      maxMessage = "Your name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=false)
     */
    private ?string $description = "";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_day", type="datetime", nullable=false)
     */
    private ?\DateTime $startDay;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_day", type="datetime", nullable=false)
     */
    private ?\DateTime $endDay;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=false)
     */
    private ?\DateTime $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=false)
     */
    private ?\DateTime $endTime;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=120, nullable=false)
     */
    private ?string $place = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_event_account", referencedColumnName="id")
     * })
     */
    private $idEventAccount;

    /**
     * @var \Categoryevent
     *
     * @ORM\ManyToOne(targetEntity="Categoryevent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_event_cat", referencedColumnName="id")
     * })
     */
    private $idEventCat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStartDay(): ?\DateTimeInterface
    {
        return $this->startDay;
    }

    public function setStartDay(\DateTimeInterface $startDay): self
    {
        $this->startDay = $startDay;

        return $this;
    }

    public function getEndDay(): ?\DateTimeInterface
    {
        return $this->endDay;
    }

    public function setEndDay(\DateTimeInterface $endDay): self
    {
        $this->endDay = $endDay;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getIdEventAccount(): ?Account
    {
        return $this->idEventAccount;
    }

    public function setIdEventAccount(?Account $idEventAccount): self
    {
        $this->idEventAccount = $idEventAccount;

        return $this;
    }

    public function getIdEventCat(): ?Categoryevent
    {
        return $this->idEventCat;
    }

    public function setIdEventCat(?Categoryevent $idEventCat): self
    {
        $this->idEventCat = $idEventCat;

        return $this;
    }
}
