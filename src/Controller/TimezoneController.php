<?php

namespace App\Controller;

use App\Entity\Timezone;
use App\Form\TimezoneType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/timezone")
 */
class TimezoneController extends AbstractController
{
    /**
     * @Route("/", name="app_timezone_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $timezones = $entityManager
            ->getRepository(Timezone::class)
            ->findAll();

        return $this->render('timezone/index.html.twig', [
            'timezones' => $timezones,
        ]);
    }

    /**
     * @Route("/new", name="app_timezone_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $timezone = new Timezone();
        $form = $this->createForm(TimezoneType::class, $timezone);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($timezone);
            $entityManager->flush();

            return $this->redirectToRoute('app_timezone_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('timezone/new.html.twig', [
            'timezone' => $timezone,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_timezone_show", methods={"GET"})
     */
    public function show(Timezone $timezone): Response
    {
        return $this->render('timezone/show.html.twig', [
            'timezone' => $timezone,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_timezone_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Timezone $timezone, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TimezoneType::class, $timezone);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_timezone_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('timezone/edit.html.twig', [
            'timezone' => $timezone,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_timezone_delete", methods={"POST"})
     */
    public function delete(Request $request, Timezone $timezone, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$timezone->getId(), $request->request->get('_token'))) {
            $entityManager->remove($timezone);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_timezone_index', [], Response::HTTP_SEE_OTHER);
    }
}
