<?php

namespace App\Controller;

use App\Entity\Categorytask;
use App\Form\CategorytaskType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categorytask")
 */
class CategorytaskController extends AbstractController
{
    /**
     * @Route("/", name="app_categorytask_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $categorytasks = $entityManager
            ->getRepository(Categorytask::class)
            ->findAll();

        return $this->render('categorytask/index.html.twig', [
            'categorytasks' => $categorytasks,
        ]);
    }

    /**
     * @Route("/new", name="app_categorytask_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $categorytask = new Categorytask();
        $form = $this->createForm(CategorytaskType::class, $categorytask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($categorytask);
            $entityManager->flush();

            return $this->redirectToRoute('app_categorytask_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('categorytask/new.html.twig', [
            'categorytask' => $categorytask,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_categorytask_show", methods={"GET"})
     */
    public function show(Categorytask $categorytask): Response
    {
        return $this->render('categorytask/show.html.twig', [
            'categorytask' => $categorytask,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_categorytask_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Categorytask $categorytask, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CategorytaskType::class, $categorytask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_categorytask_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('categorytask/edit.html.twig', [
            'categorytask' => $categorytask,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_categorytask_delete", methods={"POST"})
     */
    public function delete(Request $request, Categorytask $categorytask, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categorytask->getId(), $request->request->get('_token'))) {
            $entityManager->remove($categorytask);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_categorytask_index', [], Response::HTTP_SEE_OTHER);
    }
}
