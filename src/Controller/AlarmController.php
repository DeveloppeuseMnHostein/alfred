<?php

namespace App\Controller;

use App\Entity\Alarm;
use App\Form\AlarmType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/alarm")
 */
class AlarmController extends AbstractController
{
    /**
     * @Route("/", name="app_alarm_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $alarms = $entityManager
            ->getRepository(Alarm::class)
            ->findAll();

        return $this->render('alarm/index.html.twig', [
            'alarms' => $alarms,
        ]);
    }

    /**
     * @Route("/new", name="app_alarm_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $alarm = new Alarm();
        $form = $this->createForm(AlarmType::class, $alarm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($alarm);
            $entityManager->flush();

            return $this->redirectToRoute('app_alarm_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('alarm/new.html.twig', [
            'alarm' => $alarm,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_alarm_show", methods={"GET"})
     */
    public function show(Alarm $alarm): Response
    {
        return $this->render('alarm/show.html.twig', [
            'alarm' => $alarm,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_alarm_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Alarm $alarm, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AlarmType::class, $alarm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_alarm_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('alarm/edit.html.twig', [
            'alarm' => $alarm,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_alarm_delete", methods={"POST"})
     */
    public function delete(Request $request, Alarm $alarm, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$alarm->getId(), $request->request->get('_token'))) {
            $entityManager->remove($alarm);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_alarm_index', [], Response::HTTP_SEE_OTHER);
    }
}
