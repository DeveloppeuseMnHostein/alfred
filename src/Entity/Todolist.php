<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Todolist
 *
 * @ORM\Table(name="todolist", indexes={@ORM\Index(name="fk_cattask_idx", columns={"id_cattask"}), @ORM\Index(name="fk_account_idx", columns={"id_account"})})
 * @ORM\Entity(repositoryClass="App\Repository\TodolistRepository")
 */
class Todolist
{
    /**
     * @var int
     *
     * @ORM\Column(name=" id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your name must be at least {{ limit }} characters long",
     *      maxMessage = "Your name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $name = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id")
     * })
     */
    private $idAccount;

    /**
     * @var \Categorytask
     *
     * @ORM\ManyToOne(targetEntity="Categorytask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cattask", referencedColumnName="id")
     * })
     */
    private $idCattask;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIdAccount(): ?Account
    {
        return $this->idAccount;
    }

    public function setIdAccount(?Account $idAccount): self
    {
        $this->idAccount = $idAccount;

        return $this;
    }

    public function getIdCattask(): ?Categorytask
    {
        return $this->idCattask;
    }

    public function setIdCattask(?Categorytask $idCattask): self
    {
        $this->idCattask = $idCattask;

        return $this;
    }
}
