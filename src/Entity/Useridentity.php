<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Useridentity
 *
 * @ORM\Table(name="useridentity", uniqueConstraints={@ORM\UniqueConstraint(name="id_user_UNIQUE", columns={"id_user"})}, indexes={@ORM\Index(name="fk_languages_idx", columns={"id_language"}), @ORM\Index(name="fk_timezone_idx", columns={"id_timezone"})})
 * @ORM\Entity(repositoryClass="App\Repository\UseridentityRepository")
 */
class Useridentity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=45, nullable=false)
     */
    private ?string $surname = "";

    /**
     * @var string
     *
     * @ORM\Column(name="portrait", type="string", length=250, nullable=false, options={"comment"="url"})
     */
    private ?string $portrait = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @var \Timezone
     *
     * @ORM\ManyToOne(targetEntity="Timezone")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_timezone", referencedColumnName="id")
     * })
     */
    private $idTimezone;

    /**
     * @var \Languages
     *
     * @ORM\ManyToOne(targetEntity="Languages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_language", referencedColumnName="id")
     * })
     */
    private $idLanguage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPortrait(): ?string
    {
        return $this->portrait;
    }

    public function setPortrait(string $portrait): self
    {
        $this->portrait = $portrait;

        return $this;
    }

    public function getIdUser(): ?Account
    {
        return $this->idUser;
    }

    public function setIdUser(?Account $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getIdTimezone(): ?Timezone
    {
        return $this->idTimezone;
    }

    public function setIdTimezone(?Timezone $idTimezone): self
    {
        $this->idTimezone = $idTimezone;

        return $this;
    }

    public function getIdLanguage(): ?Languages
    {
        return $this->idLanguage;
    }

    public function setIdLanguage(?Languages $idLanguage): self
    {
        $this->idLanguage = $idLanguage;

        return $this;
    }
}
