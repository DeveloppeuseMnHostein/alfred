<?php

namespace App\Controller;

use App\Entity\Useridentity;
use App\Form\UseridentityType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/useridentity")
 */
class UseridentityController extends AbstractController
{
    /**
     * @Route("/", name="app_useridentity_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $useridentities = $entityManager
            ->getRepository(Useridentity::class)
            ->findAll();

        return $this->render('useridentity/index.html.twig', [
            'useridentities' => $useridentities,
        ]);
    }

    /**
     * @Route("/new", name="app_useridentity_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $useridentity = new Useridentity();
        $form = $this->createForm(UseridentityType::class, $useridentity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($useridentity);
            $entityManager->flush();

            return $this->redirectToRoute('app_useridentity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('useridentity/new.html.twig', [
            'useridentity' => $useridentity,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_useridentity_show", methods={"GET"})
     */
    public function show(Useridentity $useridentity): Response
    {
        return $this->render('useridentity/show.html.twig', [
            'useridentity' => $useridentity,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_useridentity_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Useridentity $useridentity, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UseridentityType::class, $useridentity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_useridentity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('useridentity/edit.html.twig', [
            'useridentity' => $useridentity,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_useridentity_delete", methods={"POST"})
     */
    public function delete(Request $request, Useridentity $useridentity, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$useridentity->getId(), $request->request->get('_token'))) {
            $entityManager->remove($useridentity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_useridentity_index', [], Response::HTTP_SEE_OTHER);
    }
}
