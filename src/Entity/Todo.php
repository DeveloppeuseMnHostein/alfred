<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Todo
 *
 * @ORM\Table(name="todo", indexes={@ORM\Index(name="fk_list_idx", columns={"id_list"}), @ORM\Index(name="fk_alarm_todo_idx", columns={"id_alarm_todo"})})
 * @ORM\Entity(repositoryClass="App\Repository\TodoRepository")
 */
class Todo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your name must be at least {{ limit }} characters long",
     *      maxMessage = "Your name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=false)
     */
    private ?string $description = "";

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_alarm_todo", type="integer", nullable=true)
     */
    private $idAlarmTodo;

    /**
     * @var \Todolist
     *
     * @ORM\ManyToOne(targetEntity="Todolist")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName=" id")
     * })
     */
    private $idList;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIdAlarmTodo(): ?int
    {
        return $this->idAlarmTodo;
    }

    public function setIdAlarmTodo(?int $idAlarmTodo): self
    {
        $this->idAlarmTodo = $idAlarmTodo;

        return $this;
    }

    public function getIdList(): ?Todolist
    {
        return $this->idList;
    }

    public function setIdList(?Todolist $idList): self
    {
        $this->idList = $idList;

        return $this;
    }
}
