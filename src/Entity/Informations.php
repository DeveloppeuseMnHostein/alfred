<?php

namespace App\Entity;

use App\Entity\Account;
use App\Entity\Categorytask;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Informations
 *
 * @ORM\Table(name="informations", indexes={@ORM\Index(name="fk_info_cat_idx", columns={"id_info_cat"}), @ORM\Index(name="fk_info_account_idx", columns={"id_info_account"})})
 * @ORM\Entity(repositoryClass="App\Repository\InformationsRepository")
 */
class Informations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=120, nullable=false)
     */
    private ?string $name = "";

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private ?string $url = "";

    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="text", length=0, nullable=true)
     */
    private ?string $text = "";

    /**
     * @var string|null
     *
     * @ORM\Column(name="image", type="string", length=120, nullable=true)
     */
    private ?string $image = "";

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=10, nullable=true)
     * 
     * @Assert\Regex(pattern="/[\+[0]]?[\+[1-7]]?[(]?[0-9]{8}[)]?[-\s\.]?$/", message="number_only")
     * 
     */
    private ?string $phone = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_info_account", referencedColumnName="id")
     * })
     */
    private $idInfoAccount;

    /**
     * @var \Categorytask
     *
     * @ORM\ManyToOne(targetEntity="Categorytask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_info_cat", referencedColumnName="id")
     * })
     */
    private $idInfoCat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getIdInfoAccount(): ?Account
    {
        return $this->idInfoAccount;
    }

    public function setIdInfoAccount(?Account $idInfoAccount): self
    {
        $this->idInfoAccount = $idInfoAccount;

        return $this;
    }

    public function getIdInfoCat(): ?Categorytask
    {
        return $this->idInfoCat;
    }

    public function setIdInfoCat(?Categorytask $idInfoCat): self
    {
        $this->idInfoCat = $idInfoCat;

        return $this;
    }
}
