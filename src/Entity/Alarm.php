<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Alarm
 *
 * @ORM\Table(name="alarm", indexes={@ORM\Index(name="fk_alarm_idx", columns={"id_alarm"}), @ORM\Index(name="fk_alarm_event_idx", columns={"id_event"})})
 * @ORM\Entity(repositoryClass="App\Repository\AlarmRepository")
 */
class Alarm
{
    /**
     * @var int
     *
     * @ORM\Column(name=" id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your login must be at least {{ limit }} characters long",
     *      maxMessage = "Your login cannot be longer than {{ limit }} characters"
     * )
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="recurrence", type="string", length=45, nullable=false)
     */
    private $recurrence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alarm_time", type="datetime", nullable=false)
     */
    private ?\DateTime $alarmTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alarm_day", type="datetime", nullable=false)
     */
    private ?\DateTime $alarmDay;

    /**
     * @var \Event
     *
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_alarm", referencedColumnName="id")
     * })
     */
    private $idAlarm;

    /**
     * @var \Event
     *
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_event", referencedColumnName="id")
     * })
     */
    private $idEvent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRecurrence(): ?string
    {
        return $this->recurrence;
    }

    public function setRecurrence(string $recurrence): self
    {
        $this->recurrence = $recurrence;

        return $this;
    }

    public function getAlarmTime(): ?\DateTimeInterface
    {
        return $this->alarmTime;
    }

    public function setAlarmTime(\DateTimeInterface $alarmTime): self
    {
        $this->alarmTime = $alarmTime;

        return $this;
    }

    public function getAlarmDay(): ?\DateTimeInterface
    {
        return $this->alarmDay;
    }

    public function setAlarmDay(\DateTimeInterface $alarmDay): self
    {
        $this->alarmDay = $alarmDay;

        return $this;
    }

    public function getIdAlarm(): ?Event
    {
        return $this->idAlarm;
    }

    public function setIdAlarm(?Event $idAlarm): self
    {
        $this->idAlarm = $idAlarm;

        return $this;
    }

    public function getIdEvent(): ?Event
    {
        return $this->idEvent;
    }

    public function setIdEvent(?Event $idEvent): self
    {
        $this->idEvent = $idEvent;

        return $this;
    }
}
