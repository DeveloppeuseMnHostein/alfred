<?php

namespace App\Controller;

use App\Entity\Categoryevent;
use App\Form\CategoryeventType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categoryevent")
 */
class CategoryeventController extends AbstractController
{
    /**
     * @Route("/", name="app_categoryevent_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $categoryevents = $entityManager
            ->getRepository(Categoryevent::class)
            ->findAll();

        return $this->render('categoryevent/index.html.twig', [
            'categoryevents' => $categoryevents,
        ]);
    }

    /**
     * @Route("/new", name="app_categoryevent_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $categoryevent = new Categoryevent();
        $form = $this->createForm(CategoryeventType::class, $categoryevent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($categoryevent);
            $entityManager->flush();

            return $this->redirectToRoute('app_categoryevent_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('categoryevent/new.html.twig', [
            'categoryevent' => $categoryevent,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_categoryevent_show", methods={"GET"})
     */
    public function show(Categoryevent $categoryevent): Response
    {
        return $this->render('categoryevent/show.html.twig', [
            'categoryevent' => $categoryevent,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_categoryevent_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Categoryevent $categoryevent, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CategoryeventType::class, $categoryevent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_categoryevent_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('categoryevent/edit.html.twig', [
            'categoryevent' => $categoryevent,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_categoryevent_delete", methods={"POST"})
     */
    public function delete(Request $request, Categoryevent $categoryevent, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categoryevent->getId(), $request->request->get('_token'))) {
            $entityManager->remove($categoryevent);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_categoryevent_index', [], Response::HTTP_SEE_OTHER);
    }
}
